In order to choose the first points correctly to use louvain on it I decided to re create the indexes of the graph's nodes from the biggest centralities to the lowest ones.
The problem is that the computation of the centralities are really expansive in terms of cpu time so I decided to compute the centrality only for a certain percentage of the nodes.
I would prefer if the chosen nodes were the ones with the biggest centralities so I assume that I can firstly compute the strength (degree of the nodes) which is likely to be highly correlated with the centrality values.
Then I am gonna sort the nodes according to their strength to get the supposed highest centralities and take only the percentage of them on which I want to compute the centrality.


The algorithm reads graphs with the edgelist format of igraph.
So for the very large graphs like inet, p2p, ip and so on, you need to delete the first line of the file indicating the number of vertices.


To compile and execute the c file:

	$ ./compile.sh

		OR

	$ ./compile.sh [epsilon]

(With epsilon being the parameter for the size of the proportion, I usually set this value between 0.00005 and 0.0001)
(The default value of epsilon is 0.0001)

If this command doesn't work because the permission is denied, you need to chmod 700 on the compile.sh file. If the error is that the script can't open your file, it means the path you gave to the graph is not correct.
