#include "compute_metrics.h"

void compute_strength(igraph_vector_t *strength_res, const igraph_t *graph)
{
    printf("\t- Computing strength on the principal connected component...\n");

    igraph_vs_t vs_strength;
    igraph_vs_all(&vs_strength);
    igraph_vector_init(strength_res, 0);
    igraph_strength(graph, strength_res, vs_strength, IGRAPH_ALL, 0, 0);
}

void compute_centrality(igraph_vector_t *centrality_res, const igraph_t *graph, const igraph_vs_t vertex_selector)
{
    igraph_vector_init(centrality_res, 0);
    igraph_closeness(graph, centrality_res, vertex_selector, IGRAPH_ALL, 0, 0);
}
