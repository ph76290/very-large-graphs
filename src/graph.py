#/usr/bin/python3

import networkx as nx
import community
import matplotlib.pyplot as plt


plt.subplot(121)
G = nx.petersen_graph()
nx.draw(G, with_labels=True, font_weight="bold")

plt.subplot(122)
ba = nx.barabasi_albert_graph(100, 5)
nx.draw(ba, with_labels=True, font_weight="bold")

plt.show()
