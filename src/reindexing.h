#include <igraph/igraph.h>

void fill_index_with_strength(igraph_vector_t *strength_res, igraph_vector_t *index, long k);
void fill_index_with_centrality(igraph_vector_t *centrality_res, const igraph_vector_t *strength_res, igraph_vector_t *centrality_index, igraph_vector_t *strength_index, long k, long nb_nodes);
