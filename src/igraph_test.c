#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "compute_metrics.h"
#include "destroyer.h"
#include "louvain.h"
#include "principal_connected_component.h"
#include "read_graph.h"
#include "reindexing.h"
#include "shuffle_graph.h"


int main(int argc, char	*argv[])
{
    igraph_rng_seed(igraph_rng_default(), 42);
    igraph_t graph_read;
    float epsilon = 0.0001;
    int centrality_number = 1;
    if (argc > 1 && atof(argv[1]) <= 1)
        epsilon = atof(argv[1]);

    printf("This program is running with the parameter epsilon: %f\n\n", epsilon);

    // READ THE GRAPH
    if (read_graph(&graph_read))
	    return 1;


   // PRINCIPAL CONNECTED COMPONENT
    printf("Computing the principal component of the graph...\n");

    long max_size = 0;
    igraph_t principal_component;
    principal_connected_component(&graph_read, &principal_component, &max_size);
    igraph_integer_t nb_edges = igraph_ecount(&principal_component);
    printf("\t- The number of vertices in the principal connected component is %ld and the number of edges is %d\n\n", max_size, nb_edges);


    // SHUFFLE THE GRAPH
    igraph_t graph;

    printf("Shuffling the graph...\n\n");
    shuffle_graph(&principal_component, &graph, max_size);


    // LOUVAIN ON PRINCIPAL COMPONENT
    printf("Executing Louvain...\n\n");

    clock_t start1, end1;
    double cpu_time_used1;
    start1 = clock();
    
    louvain(&graph);

    end1 = clock();
    cpu_time_used1 = ((double) (end1 - start1)) / CLOCKS_PER_SEC;

    printf("Louvain took %f seconds to run\n", cpu_time_used1);


    // FIND THE BEST NODES ON WHICH TO START
    // Compute the 2% biggest centrality nodes, apply the different centralities metrics on this and renumbered the indexes
    printf("Finding the best points to update the indexes of the graph nodes...\n");

    igraph_vector_t strength_res;
    compute_strength(&strength_res, &graph);

    printf("\t- Re-indexing the principal connected component with epsilon = %.6f...\n\n", epsilon);
    long k = (long)(igraph_vector_size(&strength_res) * epsilon);

    igraph_vector_t strength_index;
    igraph_vector_init(&strength_index, 0);

    fill_index_with_strength(&strength_res, &strength_index, k);

    igraph_vs_t vertex_selector;
    igraph_vs_vector(&vertex_selector, &strength_index);

    igraph_vector_t centrality_res;
    
    compute_centrality(&centrality_res, &graph, vertex_selector);

    igraph_vector_t centrality_index;
    igraph_vector_init(&centrality_index, 0);

    fill_index_with_centrality(&centrality_res, &strength_res, &centrality_index, &strength_index, k, max_size);

    igraph_t new_graph;
    igraph_permute_vertices(&graph, &new_graph, &centrality_index);
   
 
    // LOUVAIN NEW COMPUTED GRAPH
    printf("Executing Louvain...\n\n");

    clock_t start2, end2;
    double cpu_time_used2;
    start2 = clock();
    
    louvain(&new_graph);

    end2 = clock();
    cpu_time_used2 = ((double) (end2 - start2)) / CLOCKS_PER_SEC;


    // DESTROY
    destroyer(&strength_res, &centrality_res, &centrality_index, &strength_index, &new_graph, &principal_component, &graph, &graph_read);
    
    printf("This program ran successfully and Louvain took %f seconds to run\n", cpu_time_used2);
    return 0;
}
