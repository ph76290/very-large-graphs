#include "destroyer.h"

void destroyer(igraph_vector_t *strength_res,
	       igraph_vector_t *closeness_res,
	       igraph_vector_t *new_index,
	       igraph_vector_t *new_index_temp,
	       igraph_t *new_graph,
	       igraph_t *principal_component,
	       igraph_t *graph,
	       igraph_t *graph_read)
{
    igraph_vector_destroy(strength_res);
    igraph_vector_destroy(closeness_res);
    igraph_vector_destroy(new_index);
    igraph_vector_destroy(new_index_temp);
    igraph_destroy(new_graph);
    igraph_destroy(principal_component);
    igraph_destroy(graph);
    igraph_destroy(graph_read);
}
