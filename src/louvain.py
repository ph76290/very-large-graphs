import networkx
import community

graph = nx.read_edgelist("inet")

community = community.best_partition(graph)
