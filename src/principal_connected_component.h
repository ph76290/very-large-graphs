#include <igraph/igraph.h>

igraph_vector_ptr_t principal_connected_component(const igraph_t *graph, igraph_t *principal_component, long *max_size);
