#include <igraph/igraph.h>

void compute_strength(igraph_vector_t *strength_res, const igraph_t *graph);
void compute_centrality(igraph_vector_t *centrality_res, const igraph_t *graph, const igraph_vs_t vertex_selector);
