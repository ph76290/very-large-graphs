#include "louvain.h"

void louvain(const igraph_t *graph)
{
    igraph_vector_t membership;
    igraph_matrix_t memberships;
    igraph_vector_t modularity;

    igraph_vector_init(&modularity, 0);
    igraph_vector_init(&membership, 0);
    igraph_matrix_init(&memberships, 0, 0);

    igraph_community_multilevel(graph, 0, &membership, &memberships, &modularity);


    for (int i = 0; i < igraph_vector_size(&modularity); i++)
    {
	igraph_vector_t current_membership;
	igraph_vector_init(&current_membership, 0);
	igraph_matrix_get_row(&memberships, &current_membership, i);
        int nb_communities = igraph_vector_max(&current_membership);
        igraph_vector_destroy(&current_membership);

        printf("Level %d: Modularity = %f  ;  Number of communities = %d\n", i, igraph_vector_e(&modularity, i), nb_communities);
    }

    igraph_vector_destroy(&modularity);
    igraph_vector_destroy(&membership);
    igraph_matrix_destroy(&memberships);
    printf("\n");
}
