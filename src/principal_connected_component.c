#include "principal_connected_component.h"

igraph_vector_ptr_t principal_connected_component(const igraph_t *graph, igraph_t *principal_component, long *max_size)
{
    igraph_vector_ptr_t components;
    igraph_vector_ptr_init(&components, 0);
   
    igraph_decompose(graph, &components, IGRAPH_WEAK, -1, 2);
    long length = igraph_vector_ptr_size(&components); 

    for (long i = 0; i < length; i++)
    {
	igraph_t *subgraph = igraph_vector_ptr_e(&components, i);

        igraph_integer_t nb_vertices = igraph_vcount(subgraph);
        if (nb_vertices > *max_size)
	{
            *max_size = nb_vertices;
            igraph_copy(principal_component, subgraph);
	}
    }
    igraph_decompose_destroy(&components);
}


