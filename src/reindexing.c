#include "reindexing.h"


void fill_index_with_strength(igraph_vector_t *strength_res, igraph_vector_t *index, long k)
{
    for (long i = 0; i < k; i++)
    {
        long greatest_strength_pos = igraph_vector_which_max(strength_res);
        igraph_vector_push_back(index, greatest_strength_pos);
	igraph_vector_set(strength_res, greatest_strength_pos, 0);
    }
}


void fill_index_with_centrality(igraph_vector_t *centrality_res, const igraph_vector_t *strength_res, igraph_vector_t *centrality_index, igraph_vector_t *strength_index, long k, long nb_nodes)
{
    for (long i = 0; i < k; i++)
    {
        long greatest_centrality_pos = igraph_vector_which_max(centrality_res);
        igraph_vector_push_back(centrality_index, igraph_vector_e(strength_index, greatest_centrality_pos));
	igraph_vector_set(centrality_res, greatest_centrality_pos, 0);
    }

    for (long i = 0; i < nb_nodes; i++)
    {
	if (igraph_vector_e(strength_res, i) > 0)
            igraph_vector_push_back(centrality_index, i);
    }
}
