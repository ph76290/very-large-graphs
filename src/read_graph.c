#include "read_graph.h"

int read_graph(igraph_t *graph)
{
    char graph_str[30];
    printf("Type the name of the graph you want to open:\n");

    // Ask for a graph to read
    fgets(graph_str, 30, stdin);
    graph_str[strlen(graph_str) - 1] = '\0';

    FILE *fp;
    if((fp=fopen(graph_str, "rb")) == NULL) {
         printf("Cannot open file: %s\n", graph_str);
         exit(1);
    }

    igraph_integer_t n = 0;
    igraph_bool_t directed = 0;

    printf("\nReading the graph...\n");
    igraph_read_graph_edgelist(graph, fp, n, directed);

    printf("\t- The number of vertices in the graph is %d and the number of edges is %d\n\n", igraph_vcount(graph), igraph_ecount(graph));
    return 0;
}
