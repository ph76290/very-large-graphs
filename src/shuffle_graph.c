#include "shuffle_graph.h"

void shuffle_graph(igraph_t *graph, igraph_t *new_graph, long size)
{
    igraph_vector_t vec;
    igraph_vector_init_seq(&vec, 0, size - 1);
    igraph_vector_shuffle(&vec);
    igraph_permute_vertices(graph, new_graph, &vec);
}
