gcc src/igraph_test.c src/louvain.c src/principal_connected_component.c src/read_graph.c src/destroyer.c src/compute_metrics.c src/reindexing.c src/shuffle_graph.c -I/usr/local/igraph -L/usr/local/lib -ligraph -o igraph_test
LD_PRELOAD=./igraph-0.7.1/src/.libs/libigraph.so ./igraph_test $1 $2
